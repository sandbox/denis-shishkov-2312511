<?php

/**
 * @file
 * Page callbacks for Commerce Webpay.by.
 */

/**
 * Payment result callback.
 */
function commerce_webpayby_result_page_callback() {

  $order = commerce_order_load($_REQUEST['wsb_order_num']);
  if (!$order) {
    return drupal_not_found();
  }

  $payment_method = commerce_payment_method_instance_load($order->data['payment_method']);
  if (!$payment_method || $payment_method['method_id'] != 'commerce_webpayby') {
    return drupal_not_found();
  }

  // Load order and payment_method objects
  if ($feedback = commerce_webpayby_get_feedback($_REQUEST["wsb_tid"], $payment_method)) {
    // Load the payment method, since it won't be available if no $order object was passed in as argument
    if (commerce_webpayby_valid_sha1($order, $payment_method, $feedback)) {
      return commerce_webpayby_process_transaction($order, $payment_method, $feedback);
    }
  }

  return FALSE;
}

/**
 * Payment fail callback.
 */
function commerce_webpayby_fail_page_callback() {
  drupal_set_message(t('Payment unsuccessful!'), 'error');

  if (!empty($_REQUEST['wsb_order_num'])) {
    if ($order = commerce_order_load($_REQUEST['wsb_order_num'])) {
      commerce_payment_redirect_pane_previous_page($order);
      drupal_goto(commerce_checkout_order_uri($order));
    }
  }

  drupal_goto('<front>');
}

/**
 * Payment success callback.
 */
function commerce_webpayby_success_page_callback() {

  if (!($order = commerce_order_load($_REQUEST['wsb_order_num']))) {
    return drupal_not_found();
  }

  if (commerce_webpayby_result_page_callback()) {
    commerce_payment_redirect_pane_next_page($order);
  }
  else {
    commerce_payment_redirect_pane_previous_page($order);
  }
  drupal_goto(commerce_checkout_order_uri($order));
}

/**
 * Gets the Webpaybel feedback from GET / POST parameters.
 *
 * @return
 *   An associative array containing the Webpaybel feedback taken from the $_GET and
 *   $_POST superglobals, excluding 'q'.
 *   Returns FALSE if the Ds_Order parameter is missing (indicating missing or
 *   invalid Ogone feedback).
 */
function commerce_webpayby_get_feedback($transaction_id, $settings) {
  $feedback = FALSE;

  //////////////////////////////////////////////////
  // API Request (webpay.by)
  $postdata = '*API=&API_XML_REQUEST=' . urlencode('
    <?xml version="1.0" encoding="ISO-8859-1" ?>
    <wsb_api_request>
    <command>get_transaction</command>
    <authorization>
    <username>' . $settings['settings']['api_username'] . '</username>
    <password>' . $settings['settings']['api_password'] . '</password>
    </authorization>
    <fields>
    <transaction_id>' . $transaction_id . '
    </transaction_id>
    </fields>
    </wsb_api_request>
    ');

  $url = $settings['settings']['account'] == 'test' ? 'https://sandbox.webpay.by' : 'https://billing.webpay.by/';

  $curl = curl_init($url);
  curl_setopt($curl, CURLOPT_HEADER, 0);
  curl_setopt($curl, CURLOPT_POST, 1);
  curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);
  curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
  $response = curl_exec($curl);
  curl_close($curl);

  if (isset($_REQUEST['wsb_tid'])) {
    $feedback = $_GET + $_POST;
    unset($feedback['q']);
  }
  $feedback['webpay_response'] = simplexml_load_string($response);
  $feedback['status'] = $feedback['webpay_response']->fields->payment_type;

  return $feedback;
}

/**
 * Check if SHA1 in callback feedback is valid
 */
function commerce_webpayby_valid_sha1($order, $payment_method, $feedback) {

  $xml = $feedback['webpay_response'];

  $message = md5(
    $xml->fields->transaction_id .
    $xml->fields->batch_timestamp .
    $xml->fields->currency_id .
    $xml->fields->amount .
    $xml->fields->payment_method .
    $xml->fields->payment_type .
    $xml->fields->order_id .
    $xml->fields->rrn .
    $payment_method['settings']['secret_key']
  );

  if ($xml->fields->wsb_signature != $message) {
    watchdog('commerce_webpayby', 'Signature for the payment doesn\'t match', array(), WATCHDOG_ERROR);
    return FALSE;
  }

  return TRUE;
}

/**
 * Handle the response of the payment transaction.
 */
function _commerce_webpayby_response($response = NULL) {
  if ((int) $response == 1 || (int) $response == 4) {
    switch ((int) $response) {
      case 1:
        $msg = t('Операция завершена у спешно (Completed).');
      case 4:
        $msg = t('Операция авторизована и завершена (Authorized)');
    }
    $st = COMMERCE_PAYMENT_STATUS_SUCCESS;
  }
  else {
    $st = COMMERCE_PAYMENT_STATUS_FAILURE;
    switch ((int) $response) {
      case 2:
        $msg = t('Операция отклонена (Declined)');
      case 3:
        $msg = t('Операция в обработке (Pending)');
      case 5:
        $msg = t('Операция возвращена и требует повтора (Refunded)');
      case 6:
        $msg = t('Системная операция (System)');
      case 7:
        $msg = t('Сброшенная после авторизации операция (Voided)');
    }
  }

  return array(
    'code' => $st,
    'message' => $msg,
  );
}

/**
 * Get transaction with a specific Webpaybel Ds_AuthorisationCode.
 */
function commerce_webpayby_get_payment_transaction($feedback) {
  $feedback_remote_id = $feedback['wsb_tid'];

  $query = new EntityFieldQuery;

  $result = $query->entityCondition('entity_type', 'commerce_payment_transaction')
    ->propertyCondition('payment_method', 'webpayby')
    ->propertyCondition('remote_id', $feedback_remote_id)
    ->execute();

  if (!empty($result['commerce_payment_transaction']) && count($result['commerce_payment_transaction']) > 0) {
    $transaction = array_pop($result['commerce_payment_transaction']);
    return $transaction->transaction_id;
  }

  return FALSE;
}

/**
 * Save the payment transaction for the order.
 */
function commerce_webpayby_process_transaction($order, $payment_method, $feedback, $redirect = TRUE) {
  $transaction_id = commerce_webpayby_get_payment_transaction($feedback);
  if (!$transaction_id) {
    $transaction = commerce_payment_transaction_new('webpayby', $order->order_id);
  }
  else {
    $transaction = commerce_payment_transaction_load($transaction_id);
  }
  // Handle the response of the bank.
  $transaction_status = _commerce_webpayby_response($feedback['status']);

  $transaction->instance_id = $payment_method['instance_id'];
  $transaction->amount = $order->commerce_order_total[LANGUAGE_NONE][0]['amount'];
  $transaction->currency_code = $order->commerce_order_total[LANGUAGE_NONE][0]['currency_code'];
  $transaction->remote_id = $feedback['wsb_tid'];

  $transaction->status = $transaction_status['code'];
  if ($transaction_status['code'] == COMMERCE_PAYMENT_STATUS_SUCCESS) {
    $transaction->message = 'Transaction accepted with id @transaction_id';
  }
  elseif ($transaction_status['code'] == COMMERCE_PAYMENT_STATUS_FAILURE) {
    $transaction->message = 'Error for the transaction with id @transaction_id: ' . $transaction_status['message'];
  }
  $transaction->message_variables = array(
    '@transaction_id' => $feedback['wsb_tid'],
  );
  commerce_payment_transaction_save($transaction);
  return TRUE;
}
